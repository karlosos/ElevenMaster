#include "stdafx.h"
#include "Goal.h"


Goal::Goal(vec3 pos, vec3 color, float size, std::string textureName )
{
	this->pos = pos;
	this->diffuse = color;
	this->size = size;
	this->textureName = textureName;
	this->speed = 0;
	this->direction = false;

	this->radius = size / 4;
}


Goal::~Goal()
{
}

void Goal::Render()
{
	// odkomentowac jesli chce widziec 
	// prostokat do kolizji
	//drawGoalDetectionRectangle();

	glMaterialfv(GL_FRONT, GL_AMBIENT, &ambient.x);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &diffuse.x);
	glMaterialfv(GL_FRONT, GL_SPECULAR, &specular.x);

	glPushMatrix();
	// ustaw bramke
	glTranslatef(pos.x, pos.y, pos.z);
	// skaluj bramke (wysokosc o polowe mniejsza niz szerokosc)
	glScalef(1.0, 0.5f, 0.33f);
	// prawy slupek
	glPushMatrix();
	glTranslatef(size/2, 0, 0);
	glScalef(0.05f, 1.f, 0.05f);
	glutSolidCube(size);
	glPopMatrix();
	// lewy slupek
	glPushMatrix();
	glTranslatef(-size / 2, 0, 0);
	glScalef(0.05f, 1.f, 0.05f);
	glutSolidCube(size);
	glPopMatrix();
	// poprzeczka
	glPushMatrix();
	glTranslatef(0, size/2, 0);
	glScalef(1.f + 0.05, 0.1f, 0.05f);
	glutSolidCube(size);
	glPopMatrix();
	glEnd();
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
}

void Goal::drawGoalDetectionRectangle()
{
	vec3 color = vec3(1, 0.5, 0.5);
	glMaterialfv(GL_FRONT, GL_AMBIENT, &ambient.x);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &color.x);
	glMaterialfv(GL_FRONT, GL_SPECULAR, &specular.x);

	// rysuj prostokat
	// ktory obrazuje goal detection
	// domyslnie nie jest to uzywane
	// ale moge odkomentowac 26 linie w tym pliku
	glPushMatrix();
	glBegin(GL_QUADS);
	glVertex3f(-size / 2 + pos.x, size / 2, pos.z + 3.5);
	glVertex3f(-size / 2 + pos.x, 0.f, pos.z + 3.5);
	glVertex3f(size / 2 + pos.x, 0.f, pos.z + 3.5);
	glVertex3f(size / 2 + pos.x, size / 2, pos.z + 3.5);
	glEnd();
	glPopMatrix();
}

void Goal::Update()
{
	
}
