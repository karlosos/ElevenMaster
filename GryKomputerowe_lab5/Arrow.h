#pragma once
#include "SceneObject.h"
class Arrow :
	public SceneObject
{
public:
	Arrow();
	~Arrow();

	void Render();
	void Update();

	vec3 dir;
	float speed;

	float velocity_vertical;
	float velocity_horizontal;

	float direction = 0.0f;
};

