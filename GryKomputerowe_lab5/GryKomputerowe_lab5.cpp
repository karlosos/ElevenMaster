#include "stdafx.h"

void OnRender();
void OnReshape(int, int);
void OnKeyPress(unsigned char, int, int);
void OnKeyDown(unsigned char, int, int);
void OnKeyUp(unsigned char, int, int);
void OnTimer(int);
void OnMouseMove(int, int);
void OnMouseClick(int, int, int, int);

Scene scene;

vec3 mousePosition;

bool captureMouse;

bool light1state = true;
bool light2state = true;

float T = 0;

float windowResolutionX = 800;
float windowResolutionY = 600;
float windowCenterX = windowResolutionX / 2;
float windowCenterY = windowResolutionY / 2;

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	// ustawienia gluta
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(windowResolutionX, windowResolutionY);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);

	// tworzenie okna
	glutCreateWindow("ElevenMaster 1.0");

	// podlaczanie funkcji
	glutDisplayFunc(OnRender);
	glutReshapeFunc(OnReshape);
	glutKeyboardFunc(OnKeyPress);
	glutKeyboardUpFunc(OnKeyUp);
	glutTimerFunc(17, OnTimer, 0);
	glutPassiveMotionFunc(OnMouseMove);
	glutMotionFunc(OnMouseMove);
	glutMouseFunc(OnMouseClick);

	// domyslny kolor
	glClearColor(0.1f, 0.2f, 0.3f, 0.0);

	glEnable(GL_DEPTH_TEST);

	// globalny ambient
	float gl_amb[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, gl_amb);

	// wlaczenie swiatla
	glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);

	// wlaczenie cullingu
	// tylnie sciany nie sa wyswietlane
	// sciany deklarujemy przeciwnie do wskazowej zegara CCW
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);

	// wylacz kursor myszy
	glutSetCursor(GLUT_CURSOR_NONE);
	captureMouse = true;
	// wycentruj kursor myszy na srodku
	glutWarpPointer(windowCenterX, windowCenterY);

	// ograniczenia planszy gry
	// teraz nie uzywane, ale przydawalo sie do debugowania
	scene.boundaryMin = vec3(-50, 0, -50);
	scene.boundaryMax = vec3(50, 10, 50);

	// trawa
	scene.AddTriangleCollider(vec3(50, -3, -50), vec3(-50, -3, -50), vec3(50, -3, 50), vec3(1, 1), vec3(0, 1), vec3(1, 0), "grass");
	scene.AddTriangleCollider(vec3(-50, -3, -50), vec3(-50, -3, 50), vec3(50, -3, 50), vec3(0, 1), vec3(0, 0), vec3(1, 0), "grass");
	
	// sciana stadionu (trybuny)
	scene.AddTriangleCollider(vec3(35, 0, -30), vec3(35, 45, -30), vec3(-35, 45, -30), vec3(1, 0), vec3(1, 1), vec3(0, 1), "stadium");
	scene.AddTriangleCollider(vec3(-35, 45, -30), vec3(-35, 0, -30), vec3(35, 0, -30), vec3(0, 1), vec3(0, 0), vec3(1, 0), "stadium");
	
	// zresetuj obiekty na scenie
	scene.Restart();
	TextureManager::getInstance()->LoadTexture("grass", "../Resources/Textures/grass.jpg", GL_LINEAR, GL_LINEAR_MIPMAP_NEAREST);
	TextureManager::getInstance()->LoadTexture("skydome", "../Resources/Textures/skydome.bmp", GL_LINEAR, GL_LINEAR_MIPMAP_NEAREST);
	TextureManager::getInstance()->LoadTexture("stadium", "../Resources/Textures/stadium.jpg", GL_LINEAR, GL_LINEAR_MIPMAP_NEAREST);
	TextureManager::getInstance()->LoadTexture("Football", "../Resources/Textures/Football.png", GL_LINEAR, GL_LINEAR_MIPMAP_NEAREST);

	glutMainLoop();

	return 0;
}

bool keystate[256];

void OnKeyPress(unsigned char key, int x, int y) {
	if (!keystate[key]) {
		OnKeyDown(key, x, y);
	}
	keystate[key] = true;
}

void OnKeyDown(unsigned char key, int x, int y) {
	if (key == 27) {
		glutLeaveMainLoop();
	}
	if (key == 'm')
	{
		if (captureMouse)
		{
			glutSetCursor(GLUT_CURSOR_LEFT_ARROW);
			captureMouse = false;
		}
		else
		{
			glutSetCursor(GLUT_CURSOR_NONE);
			glutWarpPointer(windowCenterX, windowCenterY);
			captureMouse = true;
		}
	}
	if (key == '1')
	{
		light1state = !light1state;
	}

	if (key == '2')
	{
		light2state = !light2state;
	}

	if (key == 'r' && scene.state == Scene::State::END_GAME)
	{
		scene.Restart();
	}
}

void OnKeyUp(unsigned char key, int x, int y) {
	keystate[key] = false;
}

void OnMouseMove(int x, int y)
{
	mousePosition.x = x;
	mousePosition.y = y;
}

void OnMouseClick(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		// jezeli gracza ma wiecej niz 50 energii to strzelaj
		if (scene.player->energy >= 50)
		{
			// stworz nowa kule
			Sphere* newSphere = new Sphere(scene.arrow->pos, vec3(0.9, 0.9, 0.9), 2.f, 5);

			// jezeli pointer celnosci na zielonym polu to leci dokladnie
			// w celowanym kierunku
			if (scene.getAccuracyLevel() == Scene::AccuracyLevel::GREEN) {
				newSphere->force = scene.player->dir * 5.0f;
			}
			// jezeli w zoltym polu
			// losuj kierunek
			else if (scene.getAccuracyLevel() == Scene::AccuracyLevel::YELLOW) {
				int v1 = rand() % 6;
				vec3 dir = scene.player->dir;
				dir.x += (v1 - 3) * 0.1;
				dir.y -= (v1 - 3) * 0.1;

				newSphere->force = dir * 5.0f;
			}
			// jezeli w czerwonym polu
			// losuj kierunek (bardziej odchylony niz w zoltym)
			else {
				int v1 = rand() % 12;
				vec3 dir = scene.player->dir;
				dir.x += (v1 - 6) * 0.1;
				dir.y -= (v1 - 6) * 0.1;

				newSphere->force = dir * 5.0f;
			}
			// odejmij energie graczowi
			scene.player->energy -= 50;
			// ustaw pole zmniejszania sie wielkosci kuli
			newSphere->radiusChangePerUpdate = -0.005f;
			// dodaj kule do sceny
			scene.AddObject(newSphere);
		}
	}
}

void OnTimer(int id) {
	if (captureMouse)
	{
		// https://en.wikipedia.org/wiki/Spherical_coordinate_system
		float theta = atan2(scene.player->dir.z, scene.player->dir.x);
		float phi = asin(scene.player->dir.y);

		theta += (round(mousePosition.x) - windowCenterX) * 0.004;
		phi -= (round(mousePosition.y) - windowCenterY) * 0.004;

		if (phi > 1.4) phi = 1.4;
		if (phi < -1.4) phi = -1.4;

		if (cos(theta) * cos(phi) > -0.6 && cos(theta) * cos(phi) < 0.6) {
			scene.player->dir.x = cos(theta) * cos(phi);
			scene.player->dir.z = sin(theta) * cos(phi);
		}
		// ustaw kierunek strzalki
		scene.arrow->direction = -cos(theta) * cos(phi) * 50;
		// wycentruj myszke
		glutWarpPointer(windowCenterX, windowCenterY);
	}
	scene.Update();
	glutTimerFunc(17, OnTimer, 0);
}

void OnRender() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	float previousT = T;
	// licz fps
	T = glutGet(GLUT_ELAPSED_TIME);
	scene.hud.fps = 1.0f / (T - previousT) * 1000;

	// przypisz do hud zmienne
	// nadmiarowe, bo scene teraz trzyma playera
	// todo refactoring
	scene.hud.energy = scene.player->energy;
	scene.hud.maxEnegry = scene.player->maxEnergy;
	scene.HeadUpDisplay();

	// ustaw kamere
	gluLookAt(
		scene.player->pos.x, scene.player->pos.y, scene.player->pos.z,
		scene.player->pos.x, scene.player->pos.y, scene.player->pos.z - 1,
		0.0f, 1.0f, 0.0f
	);

	// swiatlo 1
	if (light1state)
	{
		// kolor ambientu (poswiata)
		GLfloat l0_ambient[] = { 0.2f, 0.2f, 0.2f};
		// zwykly kolor swiatla
		GLfloat l0_diffuse[] = { 1.0f, 1.0f, 1.0};
		// kolor odbicia (refleksy)
		GLfloat l0_specular[] = { 0.5f, 0.5f, 0.5f};
		// pozycja swiatla
		GLfloat l0_position[] = { 0, 0, 0, 1.0f };

		glEnable(GL_LIGHT0);
		glLightfv(GL_LIGHT0, GL_AMBIENT, l0_ambient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, l0_diffuse);
		glLightfv(GL_LIGHT0, GL_SPECULAR, l0_specular);
		glLightfv(GL_LIGHT0, GL_POSITION, l0_position);
		// wygaszanie swiatla
		glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0);
		glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.2);
		glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0);
	}
	else
	{
		glDisable(GL_LIGHT0);
	}

	// swiatlo 2
	if (light2state)
	{
		// kolor swiatla ambient (poswiata)
		GLfloat l1_ambient[] = { 0.4f, 0.4f, 0.4f };
		// kolor swiatla
		GLfloat l1_diffuse[] = { 0.5f, 0.5f, 0.5 };
		// kolor odbicia (refleksy)
		GLfloat l1_specular[] = { 0.2f, 0.2f, 0.2f };
		GLfloat l1_position[] = { -scene.player->dir.x, -scene.player->dir.y, -scene.player->dir.z, 0.0f };

		glEnable(GL_LIGHT1);
		glLightfv(GL_LIGHT1, GL_AMBIENT, l1_ambient);
		glLightfv(GL_LIGHT1, GL_DIFFUSE, l1_diffuse);
		glLightfv(GL_LIGHT1, GL_SPECULAR, l1_specular);
		glLightfv(GL_LIGHT1, GL_POSITION, l1_position);
		// wygaszanie swiatla
		glLightf(GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0);
		glLightf(GL_LIGHT1, GL_LINEAR_ATTENUATION, 0.2);
		glLightf(GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0);
	}
	else
	{
		glDisable(GL_LIGHT1);
	}

	scene.Render();

	glFlush();
	glutSwapBuffers();
	glutPostRedisplay();
}

void OnReshape(int width, int height) {

	windowResolutionX = width;
	windowCenterY = height;
	windowCenterX = width / 2;
	windowCenterY = height / 2;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, width, height);
	gluPerspective(60.0f, (float) width / height, .01f, 250.0f);
}