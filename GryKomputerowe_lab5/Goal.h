#pragma once
#include "SceneObject.h"
class Goal :
	public SceneObject
{
public:
	Goal(vec3 pos, vec3 color, float size, std::string textureName = "");
	~Goal();

	void Render();
	void Update();

	float size;
	float speed;

	std::string textureName;

	float uvMultipler;
	bool direction;

private:
	void drawGoalDetectionRectangle();
};

