#include "stdafx.h"
#include "Scene.h"


Scene::Scene(void) : accuracy(0), state(State::PLAYING)
{
	Restart();
}


Scene::~Scene(void)
{
	// usuwanie obiektow ze sceny
	for(unsigned int i = 0 ; i < sceneObjects.size() ; i++)
		delete sceneObjects[i];

	sceneObjects.clear();
}

void Scene::AddObject(SceneObject* object)
{
	sceneObjects.push_back(object);
}


void renderBitmapString(float x, float y, char* text) {
	// renderowanie tekstu
	char *c; 
	glRasterPos3f(x, y, 0);
	for (c = text; *c != '\0'; c++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
	}
}

void Scene::AddTriangleCollider(vec3 v1, vec3 v2, vec3 v3, vec3 uv1, vec3 uv2, vec3 uv3, std::string textureName)
{
	// stawianie scian
	Triangle t;

	t.v1 = v1;
	t.v2 = v2;
	t.v3 = v3;

	t.uv1 = uv1;
	t.uv2 = uv2;
	t.uv3 = uv3;

	t.textureName = textureName;

	t.n = vec3::cross(v1 - v3, v2 - v1);
	t.n = t.n.normalized();

	t.A = t.n.x;
	t.B = t.n.y;
	t.C = t.n.z;
	t.D = -(t.A * v1.x + t.B * v1.y + t.C*v1.z);

	collisionTriangles.push_back(t);
}

void Scene::Restart()
{
	// czyszczenie obiektow na scenie
	sceneObjects.clear();

	// dodanie gracza
	player = new Player();
	AddObject(player);

	// dodanie strzalki
	arrow = new Arrow();
	AddObject(arrow);

	// dodanie skydome (niewidoczny aktualnie)
	// todo poprzesuwac obiekty aby bylo widoczne niebo
	skydome = new Skydome(150, "skydome");
	goal = new Goal(vec3(0, 0, -30), vec3(1, 0, 0), 20);
	AddObject(goal);

	// ozdobna pilka 1
	Model3D* ball1 = new Model3D(vec3(-15, 0, -20), vec3(1, 1, 1));
	ball1->load("../Resources/Models/ball.object");
	ball1->textureName = "Football";
	ball1->modelTranslation = vec3(0, 0, 0);
	ball1->modelScale = vec3(2, 2, 2);
	ball1->radius *= 0.1f;
	ball1->weight = 2;
	AddObject(ball1);

	// ozdobna pilka 2
	ball1 = new Model3D(vec3(25, 0, -25), vec3(1, 1, 1));
	ball1->load("../Resources/Models/ball.object");
	ball1->textureName = "Football";
	ball1->modelTranslation = vec3(0, 0, 0);
	ball1->modelScale = vec3(2, 2, 2);
	ball1->radius *= 0.1f;
	ball1->weight = 2;
	AddObject(ball1);

	// ozdobna pilka 3
	ball1 = new Model3D(vec3(20, 0, -26), vec3(1, 1, 1));
	ball1->load("../Resources/Models/ball.object");
	ball1->textureName = "Football";
	ball1->modelTranslation = vec3(0, 0, 0);
	ball1->modelScale = vec3(2, 2, 2);
	ball1->radius *= 0.1f;
	ball1->weight = 2;
	AddObject(ball1);

	// ozdobna pilka 4
	ball1 = new Model3D(vec3(-7, 0, -4), vec3(1, 1, 1));
	ball1->load("../Resources/Models/ball.object");
	ball1->textureName = "Football";
	ball1->modelTranslation = vec3(0, 0, 0);
	ball1->modelScale = vec3(2.5, 2.5, 2.5);
	ball1->radius *= 0.1f;
	ball1->weight = 2;
	AddObject(ball1);

	// wyzerowanie statystyk
	hud.points = 0;
	hud.missed = 0;

	// zmiana stanu na gra
	state = State::PLAYING;
}

///
/// Pobiera poziom celnosci
///
Scene::AccuracyLevel Scene::getAccuracyLevel()
{
	if (fabs(accuracy) > 0 && fabs(accuracy) <= 2) {
		return AccuracyLevel::GREEN;
	}
	else if (fabs(accuracy) > 2 && fabs(accuracy) <= 7) {
		return AccuracyLevel::YELLOW;
	}
	else if (fabs(accuracy) > 7 && fabs(accuracy) <= 15) {
		return AccuracyLevel::RED;
	}
}

///
/// Wyswietla kolka reprezentujace nietrafione strzaly
///
void Scene::renderMissedShotsGUI()
{
	// wspolrzedne pierwszego kolka
	int x = 10;
	int y = 90;

	// rysuj pierwsze kolko
	if (hud.missed > 0)
		renderMissedShotsCircle(x, y, true);
	else
		renderMissedShotsCircle(x, y, false);

	// rysuj drugie kolko
	if (hud.missed > 1)
		renderMissedShotsCircle(x + 1 * 5, y, true);
	else
		renderMissedShotsCircle(x + 1 * 5, y, false);

	// rysuj trzecie kolko
	if (hud.missed > 2)
		renderMissedShotsCircle(x + 2 * 5, y, true);
	else
		renderMissedShotsCircle(x + 2 * 5, y, false);

	// rysuj czwarte kolko
	if (hud.missed > 3)
		renderMissedShotsCircle(x + 3 * 5, y, true);
	else
		renderMissedShotsCircle(x + 3 * 5, y, false);

	// rysuj piate kolko
	if (hud.missed > 4)
		renderMissedShotsCircle(x + 4 * 5, y, true);
	else
		renderMissedShotsCircle(x + 4 * 5, y, false);
	
}

void Scene::renderMissedShotsCircle(int x, int y, bool is_red)
{
	// zmienne pomocnicze do rysowania kolka
	int triangle_amount = 20;
	int radius = 2;
	GLfloat twicePi = 2.0f * PI;
	// mniejszy srodek kolka
	glBegin(GL_TRIANGLE_FAN);
	// kolor kolka
	if (!is_red)
		glColor3f(0, 1, 0);
	else 
		glColor3f(1, 0, 0)
		;
	// srodek kolka
	glVertex2f(x, y);
	// rysowanie kolka
	for (int i = 0; i <= triangle_amount; i++) {
		glVertex2f(
			x + (radius * cos(i *  twicePi / triangle_amount)),
			y + (radius * sin(i * twicePi / triangle_amount))
		);
	}
	glEnd();
	// rysowanie obramowki kolka
	glBegin(GL_TRIANGLE_FAN);
	// kolor kolka
	if (!is_red)
		glColor3f(0.3, 0.8, 0.3);
	else
		glColor3f(0.8, 0.3, 0.3);
	// srodek kolka
	glVertex2f(x, y);
	// rysowanie kolka
	for (int i = 0; i <= triangle_amount; i++) {
		glVertex2f(
			x + ((radius+0.5) * cos(i *  twicePi / triangle_amount)),
			y + ((radius+0.5) * sin(i * twicePi / triangle_amount))
		);
	}
	glEnd();
}

///
/// ekran koncowy
///
void Scene::renderSplashScreen()
{
	char text[50];
	sprintf(text, "You scored %d goals", (int)hud.points);
	glColor3f(1, 1, 1);
	renderBitmapString(37, 65, text);

	sprintf(text, "Press R to play again", (int)hud.points);
	glColor3f(1, 1, 1);
	renderBitmapString(36, 55, text);

	sprintf(text, "Press ESC to exit again", (int)hud.points);
	glColor3f(1, 1, 1);
	renderBitmapString(35, 45, text);

	sprintf(text, "Press F to Pay Respects", (int)hud.points);
	glColor3f(1, 1, 1);
	renderBitmapString(34.5, 35, text);

	// tlo
	glBegin(GL_QUADS);
	glColor3f(0.3, 0.5, 0.2);
	glVertex2f(10, 90);
	glVertex2f(10, 10);
	glVertex2f(90, 10);
	glVertex2f(90, 90);
	glEnd();
}

void Scene::updateGoalPosition()
{
	// poruszanie bramki
	float speed = static_cast<int>(hud.points / 15) * 0.1f;
	// pointer celnosci idzie w prawo
	if (goal->direction) {
		goal->pos.x += speed;
		if (goal->pos.x >= 15) {
			goal->direction = !goal->direction;
		}
	}
	// pointer celnosci idzie w lewo
	else {
		goal->pos.x -= speed;
		if (goal->pos.x <= -15) {
			goal->direction = !goal->direction;
		}
	}
}

void Scene::updatePointerPosition()
{
	// zwieksz szybkosc poruszania sie pointera
	// co 5 strzelonych punktow zwiekszaj ilosc punktow
	float accuracy_speed = 0.1f + static_cast<int>(hud.points / 5) * 0.1f;
	// pointer celnosci idzie w prawo
	if (accuracy_increasing) {
		accuracy += accuracy_speed;
		if (accuracy >= 15) {
			accuracy_increasing = !accuracy_increasing;
		}
	}
	// pointer celnosci idzie w lewo
	else {
		accuracy -= accuracy_speed;
		if (accuracy <= -15) {
			accuracy_increasing = !accuracy_increasing;
		}
	}
}

void Scene::HeadUpDisplay()
{
	// zmiana wyswietlania na projekcje
	// 2d
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, 100, 0, 100);
	glDisable(GL_LIGHTING);
	glDisable(GL_CULL_FACE);

	if (state == State::END_GAME) {
		// wyswietl splash screen konca gry
		renderSplashScreen();
	}

	renderMissedShotsGUI();
	renderEnergyBar();
	renderAccuracyBar();

	// wyswietlanie ilosci punktow w prawym gornym rogu
	// todo przeniesc do funkcji
	char text[50];
	sprintf(text, "%d points", (int) hud.points);
	glColor3f(1, 0, 0);
	renderBitmapString(80, 90, text);

	glEnable(GL_LIGHTING);
	glEnable(GL_CULL_FACE);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

///
/// wyswietlanie paska energii na dole
///
void Scene::renderEnergyBar()
{
	glBegin(GL_QUADS);
	// zielony pasek
	glColor3f(0, 1, 0);
	glVertex2f(45, 3.5);
	glVertex2f(45, 1.5);
	glVertex2f(45 + 10 * (hud.energy / hud.maxEnegry), 1.5);
	glVertex2f(45 + 10 * (hud.energy / hud.maxEnegry), 3.5);

	// czarny pasek
	glColor3f(0, 0, 0);
	glVertex2f(45 + 10 * (hud.energy / hud.maxEnegry), 3.5);
	glVertex2f(45 + 10 * (hud.energy / hud.maxEnegry), 1.5);
	glVertex2f(55, 1.5);
	glVertex2f(55, 3.5);

	// obramowka
	glColor3f(0, 0.3, 0);
	glVertex2f(44, 4.5);
	glVertex2f(44, 0.5);
	glVertex2f(56, 0.5);
	glVertex2f(56, 4.5);
	glEnd();
}

///
/// wyswietlanie paska celnosci na dole
///
void Scene::renderAccuracyBar()
{
	// accuracy bar
	// pointer celnosc
	glBegin(GL_TRIANGLES);
	glColor3f(0, 0, 0);
	glVertex2f(48 + accuracy, 12);
	glVertex2f(50 + accuracy, 10);
	glVertex2f(52 + accuracy, 12);

	glColor3f(0.3, 0.3, 0.3);
	glVertex2f(47 + accuracy, 12.5);
	glVertex2f(50 + accuracy, 9.5);
	glVertex2f(53 + accuracy, 12.5);
	glEnd();
	// kolorowe paski pokazujace celnosc
	// rysowany jeden na drugim
	// green bar
	glBegin(GL_QUADS);
	glColor3f(0, 0.8, 0);
	glVertex2f(48, 10);
	glVertex2f(48, 5);
	glVertex2f(52, 5);
	glVertex2f(52, 10);
	// orange bar
	glColor3f(1, 0.5, 0);
	glVertex2f(43, 10);
	glVertex2f(43, 5);
	glVertex2f(57, 5);
	glVertex2f(57, 10);
	// red bar
	glColor3f(1, 0, 0);
	glVertex2f(35, 10);
	glVertex2f(35, 5);
	glVertex2f(65, 5);
	glVertex2f(65, 10);
	glEnd();
}

void Scene::Render()
{
	for(unsigned int i = 0 ; i < sceneObjects.size() ; i++)
		sceneObjects[i]->Render();

	// rysowanie scian
	for (unsigned int i = 0 ; i < collisionTriangles.size() ; i++)
	{
		if (!collisionTriangles[i].textureName.empty())
		{
			glEnable(GL_TEXTURE_2D);
			TextureManager::getInstance()->BindTexture(collisionTriangles[i].textureName);
		}

		float ambient[] = { 0.5f, 0.5f, 0.5f };
		float diffuse[] = { 1.0f, 1.0f, 1.0f };
		float specular[] = { 0.0f, 0.0f, 0.0f };

		glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
		glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
		glBegin(GL_TRIANGLES);
		glNormal3f(collisionTriangles[i].n.x, collisionTriangles[i].n.y, collisionTriangles[i].n.z);
		glTexCoord2f(collisionTriangles[i].uv1.x, collisionTriangles[i].uv1.y);
		glVertex3f(collisionTriangles[i].v1.x, collisionTriangles[i].v1.y, collisionTriangles[i].v1.z);
		glTexCoord2f(collisionTriangles[i].uv2.x, collisionTriangles[i].uv2.y);
		glVertex3f(collisionTriangles[i].v2.x, collisionTriangles[i].v2.y, collisionTriangles[i].v2.z);
		glTexCoord2f(collisionTriangles[i].uv3.x, collisionTriangles[i].uv3.y);
		glVertex3f(collisionTriangles[i].v3.x, collisionTriangles[i].v3.y, collisionTriangles[i].v3.z);
		glEnd();

		glDisable(GL_TEXTURE_2D);
	}

	skydome->Render();
}

void Scene::Update()
{
	// jezeli 5 nietrafionych strzalow wtedy koncz gre
	if (hud.missed > 4) {
		state = State::END_GAME;
	}

	// jezeli stan to gra
	if (state == State::PLAYING) {
		updatePointerPosition();
		updateGoalPosition();

		for (int i = 0; i < sceneObjects.size(); i++)
		{
			SceneObject* obj = sceneObjects[i];

			obj->prevPos = obj->pos;

			obj->pos.y -= 0.3f;

			// spraw zeby elementy nie mogly spasc poza mape (pod podloge itd)
			if ((obj->pos.x + obj->radius) > boundaryMax.x)
				obj->pos.x = boundaryMax.x - obj->radius;

			if ((obj->pos.y + obj->radius) > boundaryMax.y)
				obj->pos.y = boundaryMax.y - obj->radius;

			if ((obj->pos.z + obj->radius) > boundaryMax.z)
				obj->pos.z = boundaryMax.z - obj->radius;

			if ((obj->pos.x - obj->radius) < boundaryMin.x)
				obj->pos.x = boundaryMin.x + obj->radius;

			if ((obj->pos.y - obj->radius) < boundaryMin.y)
				obj->pos.y = boundaryMin.y + obj->radius;

			if ((obj->pos.z - obj->radius) < boundaryMin.z)
				obj->pos.z = boundaryMin.z + obj->radius;

			//
			// detekcja strzelonej bramki
			//
			float size = goal->size;
			// jezeli przekroczyla linie bramki
			if (obj->pos.z < goal->pos.z + 3.5 && obj->pos.z > goal->pos.z && obj->isActive) {
				if (obj->pos.x > -size / 2 + goal->pos.x && obj->pos.x < size / 2 + goal->pos.x) {
					obj->isActive = false;
					hud.points += 1;
					std::cout << "MAMY TO!" << std::endl;
				}
				else {
					obj->isActive = false;
					hud.missed += 1;
					std::cout << "NIE TRAFIONE!" << std::endl;
				}
			}
		}

		// aktualizacja obiektow
		for (unsigned int i = 0; i < sceneObjects.size(); i++)
			sceneObjects[i]->Update();

		// usuwanie martwych obiektow
		for (std::vector<SceneObject*>::iterator it = sceneObjects.begin(); it != sceneObjects.end();)
		{
			if ((*it)->isAlive)
			{
				it++;
			}
			else
			{
				delete (*it);
				it = sceneObjects.erase(it);
			}
		}

		// aktualizacja skydome
		// aktualnie nie widac bo zaslania sciana stadionu (trybun)
		skydome->Update();
	}
}