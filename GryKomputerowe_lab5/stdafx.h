#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <vector>
#include <map>
#include <iostream>

#include "GL\freeglut.h"
#include "FreeImage.h"

#include "vec3.h"

#include "TextureManager.h"
#include "SceneObject.h"
#include "Sphere.h"
#include "Model3D.h"
#include "Player.h"
#include "Goal.h"
#include "Arrow.h"

#include "Scene.h"

#include "Skydome.h"