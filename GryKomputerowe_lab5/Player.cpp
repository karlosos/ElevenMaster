#include "stdafx.h"
#include "Player.h"


Player::Player()
{
	// domyslna pozycja gracza
	pos.x = 0.0f;
	pos.y = 1.0f;
	pos.z = 10.0f;

	// kierunek gracza (kamery tez)
	dir.x = 0.0f;
	dir.y = 0.0f;
	dir.z = -1.0f;

	// radius gracza (do kolizji)
	// akurat nie uzywam
	radius = 2.0f;

	// energia gracza
	energy = 100;
	maxEnergy = 100;
}


Player::~Player()
{
}

void Player::Render()
{
	
}

void Player::Update()
{
	// wysokosc playera
	// inaczej by spadal (przez grawitacje)
	pos.y = 9.0;

	// regeneracja energii
	energy += 2.f;

	// maksymalne ograniczenie energii
	if (energy > maxEnergy)
		energy = maxEnergy;
}