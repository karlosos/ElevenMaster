#include "stdafx.h"
#include "Arrow.h"


Arrow::Arrow()
{
	// pozycja strzalki
	pos.x = 0.0f;
	pos.y = 0.0f;
	pos.z = -8.0f;

	// kierunek strzalki
	dir.x = 0.0f;
	dir.y = 0.0f;
	dir.z = -1.0f;

	// kolor strzalki
	this->diffuse = vec3(0, 0, 1);

	speed = 0.1f;
	radius = 2.0f;

	// predkosc musi byc
	// bo inaczej znika strzalka
	velocity_horizontal = 0;
	velocity_vertical = 0;

	weight = 2;
}


Arrow::~Arrow()
{
}

void Arrow::Render()
{
	// kolor strzalki
	glMaterialfv(GL_FRONT, GL_AMBIENT, &ambient.x);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, &diffuse.x);
	glMaterialfv(GL_FRONT, GL_SPECULAR, &specular.x);

	glPushMatrix();
	// obroc strzalke w kierunku gdzie celuje gracza
	glRotatef(direction, 0, 1, 0);

	// grot strzalki
	glPushMatrix();
	// przesun grot
	glTranslatef(pos.x, pos.y, pos.z - 2);
	// obroc grot o 180 bo domyslnie szczyt
	// skierowany jest ku graczowi
	glRotatef(180, 1, 0, 0);
	// wyswietl stozek
	glutSolidCone(2, 15, 6, 6);
	glPopMatrix();
	// pozycja strzalki
	glTranslatef(pos.x, pos.y, pos.z);
	// skaluj strzalke
	glScalef(1.f, 1.f, 2.4f);
	// prostopadloscian (podstawa strzalki)
	glutSolidCube(2);
	glEnd();
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
}

void Arrow::Update()
{
	// ograniczenia ruchu strzalki
	if (direction > 30.f)
		direction = 30.f;
	else if (direction < -30.f)
		direction = -30.f;

	// wysokosc strzalki
	pos.y = 3;
}
