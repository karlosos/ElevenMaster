Projekt wzorowany na projekcie od M Chewsiuk: http://mchwesiuk.pl/gry-komputerowe-2018/

Sterowanie:
Lewy przycisk myszy - strzela do bramki
ESC - wychodzi z gry
1 - przelacza swiatlo 1
2 - przelacza swiatlo 2
r - resetuje gre (gdy w trybie konca gry)

Zasady:
Strzel jak najwieksza ilosc bramek. 
Po 5 nietrafionych bramkach gra sie konczy.
Co 5 strzelonych bramek wskaznic celnosci porusza sie szybciej.
Co 15 strzelonych bramek, bramka porusza sie szybciej.